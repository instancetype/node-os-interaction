/**
 * Created by instancetype on 6/25/14.
 */
var http = require('http')

var server = http.createServer(function(req, res) {
  res.writeHead('Content-Type', 'text/html')
  res.end('<h1>SIGINT Listener</h1>')
})

server.listen(3000)

process.on('SIGINT', function() {

  console.log('Ctrl-C was pressed')
  server.close()
  process.exit(0)
})