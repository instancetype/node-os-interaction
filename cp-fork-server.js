/**
 * Created by instancetype on 6/27/14.
 */
var http = require('http')
  , cp = require('child_process')

var server = http.createServer(function(req, res) {
  var child = cp.fork('./cp-fork-fib_process', [req.url.substring(1)])

  child.on('message', function(m) {
    res.end(m.result + '\n')
  })
}).listen(3000)