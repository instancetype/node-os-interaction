/**
 * Created by instancetype on 6/25/14.
 */
var debug

if (process.env.DEBUG) {
  debug = function(data) {
    console.error(data)
  }
} else {
  debug = function() {}
}

debug('This is a debug call')
console.log('This is a console.log call')
debug('This is another debug call')