/**
 * Created by instancetype on 6/27/14.
 */
var fs = require('fs')

module.exports = function move(oldPath, newPath, callback) {

  fs.rename(oldPath, newPath, function(err) {
    if (err) {
      if (err.code === 'EXDEV') {
        copy()
      } else {
        callback(err)
      }
      return
    }
    callback()
  })

  function copy() {
    var readStream = fs.createReadStream(oldPath)
      , writeStream = fs.createWriteStream(newPath)

    readStream.on('error', callback)
    writeStream.on('error', callback)

    readStream.on('close', function() {
      fs.unlink(oldPath, callback)
    })
    readStream.pipe(writeStream)
  }
}