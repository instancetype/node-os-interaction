/**
 * Created by instancetype on 6/27/14.
 */
function fib(n) {
  if (n < 2) return n
  else return fib(n-2) + fib(n-1)
}

var input = parseInt(process.argv[2], 10)
process.send({ result: fib(input) })