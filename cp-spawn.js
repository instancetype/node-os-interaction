/**
 * Created by instancetype on 6/27/14.
 */
var cp = require('child_process')
  , child = cp.spawn('ls', ['-1'])

// script can interact with child.stdin, child.stdout, child.stderr
child.stdout.pipe(fs.createWriteStream('ls-result.txt'))

child.on('exit', function(code, signal) {
  // ...
})