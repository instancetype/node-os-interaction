/**
 * Created by instancetype on 6/25/14.
 */
process.on('uncaughtException', function(err) {
  console.error('Uncaught Exception:', err.message)
  process.exit(1)
})

throw new Error('an uncaught exception')